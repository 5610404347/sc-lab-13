package question2;

public class RemoveRunnable implements Runnable{
	private Contact contact;
	private int count;
	private String n;
	private static int DELAY = 1;
	
	public RemoveRunnable(Contact contact, int count, String n){
		this.contact = contact;
		this.count = count;
		this.n = n;
	}

	@Override
	public void run() {
		try{
			for(int i = 1; i <= count; i++){
				contact.remove(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}
	
	
}
